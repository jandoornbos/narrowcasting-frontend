// Directive to calculate the body size
angular.module('app').directive('screenBody', ['$window', function($window) {
    return {
        link: function(scope, element, attrs) {
            // Get the screen height
            var height = $window.innerHeight;
            // Remove the 4% paddings
            var paddings = height * 0.04;
            // Remove all values
            // 240 = header height
            // 80 = footer height
            // 100 = body margins
            var newHeight = height - paddings - 240 - 80 - 100;
            element.css('height', newHeight);
        }
    }
}]);