// Directive to calculate message block height
angular.module('app').directive('messageBody', ['$window', function($window) {
    return {
        link: function(scope, element, attrs) {
            // Get the parent height
            var height = element.parent.innerHeight;
			// Remove the 140px from the bar (100 height 40 margin)
            var newHeight = height - 140;
            element.css('height', newHeight);
        }
    }
}]);