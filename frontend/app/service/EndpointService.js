angular.module('app').factory('EndpointService', function() {
    var baseUrl = "http://localhost:8080/narrowcasting";
    var endpoints = {};
    endpoints.getMessages = function() {
        return baseUrl + "/api/v1/message";
    };
    endpoints.getActiveEvents = function() {
		return baseUrl + "/api/v1/event?status=1";
    };
	endpoints.getTeachers = function() {
        return baseUrl + "/api/v1/teacher";
    };
	endpoints.getSetting = function(settingKey) {
	    return baseUrl + "/api/v1/setting/" + settingKey;
    };
    return endpoints;
});