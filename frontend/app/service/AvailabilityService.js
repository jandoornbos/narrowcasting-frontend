angular.module('app').factory('AvailabilityService', ['$http', 'EndpointService', function($http, EndpointService) {
    var service = {};
    service.getTeachers = function() {
        return $http({
            method: 'GET',
            url: EndpointService.getTeachers()
        });
    };
    return service;
}]);