angular.module('app').factory('FeedService', ['$http', function($http) {
    return {
        parseFeed : function(url) {
            return $http.jsonp(url);
        }
    }
}]);