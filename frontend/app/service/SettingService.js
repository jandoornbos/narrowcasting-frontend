angular.module('app').factory('SettingService', ['$http', 'EndpointService', function($http, EndpointService) {
    var service = {};
    service.getSetting = function(settingKey) {
        return $http({
            method: 'GET',
            url: EndpointService.getSetting(settingKey)
        });
    };
    return service;
}]);