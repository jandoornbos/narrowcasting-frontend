angular.module('app').factory('ClockService', ['$timeout', function($timeout) {
    var timeInterval = 1000; //ms
    var clock = {};
    clock.setCallback = function(callback) {
        clock.callback = callback;
    };

    var tick = function() {
        clock.callback();
        $timeout(tick, timeInterval);
    };

    var days = [
        "zondag",
        "maandag",
        "dinsdag",
        "woensdag",
        "donderdag",
        "vrijdag",
        "zaterdag"
    ];

    var months = [
        "januari",
        "februari",
        "maart",
        "april",
        "mei",
        "juni",
        "juli",
        "augustus",
        "september",
        "oktober",
        "november",
        "december"
    ];

    clock.formatDate = function(date) {
        return days[date.getDay()] + " " + date.getDate() + " " + months[date.getMonth()] + " " + date.getFullYear();
    };

    $timeout(tick, timeInterval);

    return clock;
}]);