angular.module('app').factory('MessageService', ['$http', 'EndpointService', function($http, EndpointService) {
    var service = {};
    service.getMessages = function() {
        return $http({
            method: 'GET',
            url: EndpointService.getMessages()
        });
    };
    return service;
}]);