angular.module('app').factory('EventService', ['$http', 'EndpointService', function($http, EndpointService) {
    var service = {};
    service.getActiveEvents = function() {
        return $http({
            method: 'GET',
            url: EndpointService.getActiveEvents()
        });
    };
    return service;
}]);