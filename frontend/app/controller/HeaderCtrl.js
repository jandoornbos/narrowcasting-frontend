angular.module('app').controller('HeaderCtrl', ['ClockService', function(ClockService) {
    var vm = this;

    var tick = function() {
        var date = new Date();
        vm.hours = date.getHours();
        vm.minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
        vm.date = ClockService.formatDate(date);
    };

    // Run function when opening
    tick();
    // Set ticker
    ClockService.setCallback(tick);

}]);