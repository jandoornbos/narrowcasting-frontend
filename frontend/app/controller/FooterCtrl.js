angular.module('app').controller('FooterCtrl', ['FeedService', '$interval', function(FeedService, $interval) {
    var vm = this;

    getFeed(); // Call at startup
    function getFeed() {
        FeedService.parseFeed('https://tweakers.net/feeds/nieuws.json?jsonp=JSON_CALLBACK').then(function success(response) {
            vm.newsfeed = [];
            for(var i = 0; i < 5; i++) {
                vm.newsfeed.push(response.data[i]);
            }
        }, function error(response) {
            console.log("FooterCtrl error: " + response);
        });
    }

    $interval(getFeed, 600 * 1000); // 600 seconds, 10 minutes
}]);