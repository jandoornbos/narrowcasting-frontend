angular.module('app').controller('AvailabilityCtrl', ['AvailabilityService', '$interval', function(AvailabilityService, $interval) {
    var vm = this;
	
	vm.toggleTeacher = function(){
			AvailabilityService.getTeachers().then(function success(response) {
			vm.teachers = response.data;
		});
	};
	vm.toggleTeacher();
	$interval(vm.toggleTeacher, 1500);
}]);