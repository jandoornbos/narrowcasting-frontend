angular.module('app').controller('MessageCtrl', ['MessageService', 'EventService', '$interval', '$timeout', 'SettingService', function(MessageService, EventService, $interval, $timeout, SettingService) {
    var vm = this;
	vm.blockDisplayMs = 3 * 1000; //SettingService.messageLoopTime * 1000; // 1000 = 1 second
	// Default values for init (incase of bad database/api
	vm.currentBlock = {title:"Data ophalen...", content:"Wanneer dit klaar is zal het worden weergegeven....", creationDate: + new Date(), secondDate: "", location: ""};
	vm.currentCount = 0;
	vm.nextBlocks = [];
	vm.showBlocks = true;
	vm.showEventData = false;
	vm.blocks = [];
	vm.cycleStarted = false;

	var messageInterval;

	// Get messages and events from the API
	vm.getData = function(){
		vm.tBlocks = [];
		MessageService.getMessages().then(function success(response) {
			for (var i = 0; i < response.data.length; i++) {
				tBlock = {};
				tBlock.type = "message";
				tBlock.title = response.data[i].title;
				tBlock.content = response.data[i].content;
				tBlock.creationDate = response.data[i].creationDate;
				tBlock.secondDate = response.data[i].expirationDate;
				tBlock.location = "";
				vm.tBlocks.push(tBlock);
			}
			vm.tBlocks.sort(vm.compareBlocks);
			if(vm.cycleStarted == false) {
				vm.cycleStarted = true;
				vm.nextBlock();
			}
		});
		EventService.getActiveEvents().then(function success(response) {
			for (var i = 0; i < response.data.length; i++) {
				tBlock = {};
				tBlock.type = "event";
				tBlock.title = response.data[i].title;
				tBlock.content = response.data[i].content;
				tBlock.creationDate = response.data[i].creationDate;
				tBlock.secondDate = response.data[i].eventDate;
				tBlock.location = response.data[i].location;
				vm.tBlocks.push(tBlock);
			}
			vm.tBlocks.sort(vm.compareBlocks);
			if(vm.cycleStarted == false) {
				vm.cycleStarted = true;
				vm.nextBlock();
			}
		});
		SettingService.getSetting('message-loop-time').then(function success(response) {
			vm.blockDisplayMs = (response.data.value * 1000);
		});
	};
	vm.compareBlocks = function(a,b) {
		if (a.secondDate < b.secondDate)
			return -1;
		if (a.secondDate > b.secondDate)
			return 1;
		return 0;
	};
	vm.nextBlock = function(){
		// We only want to change the visible data once the cycle is done (before the first item shows again)
		// If the next block is the last one in the array, prepare the new data
		if(vm.currentCount == (vm.blocks.length - 2)) {
			vm.getData();
		}
		// If the next block is the first block of the cycle, replace the visible data with the prepared data
		if(vm.currentCount == 0 || vm.blocks.length == 0) {
			vm.blocks = vm.tBlocks;
		}
		// Fade out and calculate new data
		vm.showBlocks = false;
		vm.showEventData = false;
		if(vm.blocks.length > (vm.currentCount + 1)) {
			vm.currentCount++;
		}
		else {
			vm.currentCount = 0;
		}
		
		vm.tNextBlocks = [];
		// Special loop which loops throught the array with an offset (vm.currentCount)
		for( var i = 0; i < (vm.blocks.length); i++) {
			// If there are 5 or less blocks in the queue, add them to the 'queue'
			if(vm.tNextBlocks.length <= 5 && i != 0) {
				var pointer = (i + vm.currentCount) % vm.blocks.length;
				vm.tNextBlocks.push(vm.blocks[pointer]);
			}
		}
		// After a timeout the data will be changed and the html will fade back in
		$timeout(function() {
			vm.currentBlock = vm.blocks[vm.currentCount];
			vm.nextBlocks = vm.tNextBlocks;
			vm.showBlocks = true;
			if(vm.currentBlock != null) { 
				if(vm.currentBlock.location != "") {
					vm.showEventData = true;
				}
			}
			vm.showBlocks = true;
		}, 600);
		$timeout(vm.nextBlock, vm.blockDisplayMs); 
	};
	
	// This cycle makes sure the screen will show the data as soon as Tomcat has started/is done deploying.. or however that works..
	vm.startupCycle = function(){
		vm.getData();
		if(vm.cycleStarted == false) {
			// Failed to get data, try again in a bit.
			$timeout(function() {
				vm.startupCycle($timeout);
			}, 1500);
		}
	}
	vm.startupCycle($timeout);
	//Start everything
	vm.startupCycle();
}]);