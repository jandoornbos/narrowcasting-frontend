'use strict';

angular.module('app', [
    'ngAnimate',
    'ngRoute',
    'ngSanitize',
    'ui.router'
]).config(['$routeProvider', '$stateProvider', function($routeProvider, $stateProvider) {
    $routeProvider.otherwise('/');
    $stateProvider
        .state('screen', {
            url : '',
            views: {
                'header' : {
                    templateUrl : 'partials/header.html',
                    controller : 'HeaderCtrl as vm'
                },
                'footer' : {
                    templateUrl : 'partials/footer.html',
                    controller : 'FooterCtrl as vm'
                },
                'messages' : {
                    templateUrl : 'partials/messages.html',
                    controller: 'MessageCtrl as vm'
                },
                'presence' : {
                    templateUrl : 'partials/presence.html',
                    controller: 'AvailabilityCtrl as vm'
                }
            }
        });
}]);