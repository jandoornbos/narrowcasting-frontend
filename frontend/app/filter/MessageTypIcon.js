angular.module('app').filter('toicon', function() {
    return function(input) {
        if (input == 'event') {
            return '<i class="fa fa-calendar fa-5x"></i>';
        } else {
            return '<i class="fa fa-comment fa-5x"></i>'
        }
    };
});