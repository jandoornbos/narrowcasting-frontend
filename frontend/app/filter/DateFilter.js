angular.module('app').filter('date', function() {

    var months = [
        "januari",
        "februari",
        "maart",
        "april",
        "mei",
        "juni",
        "juli",
        "augustus",
        "september",
        "oktober",
        "november",
        "december"
    ];

    return function(input) {
        var date = new Date(input);

        return date.getDate() + ' ' + months[date.getMonth()] + ' ' +
            date.getFullYear() + '  ' + date.getHours() + ':' +
            (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    };
});