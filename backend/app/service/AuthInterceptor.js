angular.module('app').factory('AuthInterceptor', ['$q', '$injector', function($q, $injector) {
    var service = this;

    service.request = function(config) {
        var endpoints = $injector.get('EndpointService');
        var tokenService = $injector.get('TokenService');
        if (config.url != endpoints.login()) {
            // Inject Authorization header
            config.headers.Authorization = tokenService.getToken();
        }
        return config;
    };

    service.responseError = function(response) {
        var endpoints = $injector.get('EndpointService');
        if (response.status == 401) {
            if (response.config.url != endpoints.login()) {
                var deferred = $q.defer();

                var tokenService = $injector.get('TokenService');
                var login = $injector.get('LoginService');
                login.refreshToken(tokenService.getRefresh()).then(function success() {
                    // Retry request
                    retryRequest(response.config, deferred);
                }, function failure(response) {
                    // Logout user
                    tokenService.removeToken();
                    $injector.get('$state').go('login');
                });

                return deferred.promise;
            }
        }
        return $q.reject(response);
    };

    function retryRequest(config, deferred) {
        function successCallback(response) {
            deferred.resolve(response);
        }
        function errorCallback(response) {
            deferred.reject(response);
        }
        var $http = $injector.get('$http');
        $http(config).then(successCallback, errorCallback);
    }

    return service;
}]);

angular.module('app').config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
}]);