angular.module('app').factory('MessageService', ['$http', 'EndpointService', function($http, EndpointService) {
    var service = {};
    service.getMessages = function(status) {
        return $http({
            method: 'GET',
            url: EndpointService.getMessages(status)
        });
    };
    service.createMessage = function(message) {
        return $http({
            method: 'POST',
            url: EndpointService.createMessage(),
            data: message
        });
    };
    service.getMessage = function(id) {
        return $http({
            method: 'GET',
            url: EndpointService.getMessage(id)
        });
    };
    service.updateMessage = function(id, message) {
        return $http({
            method: 'PATCH',
            url: EndpointService.updateMessage(id),
            data: message
        });
    };
    service.deleteMessage = function(id) {
        return $http({
            method: 'DELETE',
            url: EndpointService.getMessage(id)
        });
    };
    return service;
}]);