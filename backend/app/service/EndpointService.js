angular.module('app').factory('EndpointService', function() {
    var baseUrl = "http://localhost:8080/narrowcasting";
    var endpoints = {};
    endpoints.getMessages = function(status) {
		if(status == 1) {
			return baseUrl + "/api/v1/message?status=1";
		}
		else if(status == 0) {
			return baseUrl + "/api/v1/message?status=0";
		}
		else {
			return baseUrl + "/api/v1/message";
		}
    };
    endpoints.createMessage = function() {
    	return baseUrl + "/api/v1/message";
	};
    endpoints.getMessage = function(id) {
    	return baseUrl + "/api/v1/message/" + id;
	};
    endpoints.updateMessage = function(id) {
    	return baseUrl + "/api/v1/message/" + id;
	};
    endpoints.getEvents = function(status) {
		if(status == 1) {
			return baseUrl + "/api/v1/event?status=1";
		}
		else if(status == 0) {
			return baseUrl + "/api/v1/event?status=0";
		}
		else {
			return baseUrl + "/api/v1/event/";
		}
    };
    endpoints.createEvent = function() {
    	return baseUrl + "/api/v1/event"
	};
    endpoints.getEvent = function(id) {
    	return baseUrl + "/api/v1/event/" + id;
	};
    endpoints.getTeacher = function(id) {
        return baseUrl + "/api/v1/teacher/" + id;
    };
    endpoints.getTeachers = function() {
        return baseUrl + "/api/v1/teacher";
    };
    endpoints.createTeacher = function() {
    	return baseUrl + "/api/v1/teacher";
	};
	endpoints.updateTeacher = function() {
    	return baseUrl + "/api/v1/teacher";
	};
	endpoints.toggleTeacher = function(id) {
        return baseUrl + "/api/v1/teacher/" + id + "?toggle=1";
    };
    endpoints.deleteTeacher = function(id) {
        return baseUrl + "/api/v1/teacher/" + id;
    };
    endpoints.getSetting = function(id) {
        return baseUrl + "/api/v1/setting/" + id;
    };
    endpoints.getSettings = function() {
        return baseUrl + "/api/v1/setting";
    };
	endpoints.updateSetting = function(id) {
    	return baseUrl + "/api/v1/setting/" + id;
	};
	endpoints.login = function() {
        return baseUrl + "/api/v1/auth/token";
    };
	endpoints.getUsers = function() {
		return baseUrl + "/api/v1/user";
	};
	endpoints.createUser = function() {
		return baseUrl + "/api/v1/user";
	};
	endpoints.singleUser = function(id) {
		return baseUrl + "/api/v1/user/" + id;
	};
    return endpoints;
});