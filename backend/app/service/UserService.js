angular.module('app').factory('UserService', ['$http', 'EndpointService', function($http, EndpointService) {
    var service = {};
    service.getUsers = function() {
        return $http({
            method: 'GET',
            url: EndpointService.getUsers()
        });
    };
    service.createUser = function(user) {
        return $http({
            method: 'POST',
            url: EndpointService.createUser(),
            data: user
        });
    };
    service.getUser = function(id) {
        return $http({
            method: 'GET',
            url: EndpointService.singleUser(id)
        });
    };
    service.updateUser = function(id, user) {
        return $http({
            method: 'PUT',
            url: EndpointService.singleUser(id),
            data: user
        });
    };
    return service;
}]);