angular.module('app').factory('EventService', ['$http', 'EndpointService', function($http, EndpointService) {
    var service = {};
    service.getEvents = function(status) {
        return $http({
            method: 'GET',
            url: EndpointService.getEvents(status)
        });
    };
    service.createEvent = function(event) {
        return $http({
            method: 'POST',
            url: EndpointService.createEvent(),
            data: event
        })
    };
    service.getEvent = function (id) {
        return $http({
            method: 'GET',
            url: EndpointService.getEvent(id)
        });
    };
    service.updateEvent = function(id, event) {
        return $http({
            method: 'PUT',
            url: EndpointService.getEvent(id),
            data: event
        });
    };
    service.deleteEvent = function(id) {
        return $http({
            method: 'DELETE',
            url: EndpointService.getEvent(id)
        });
    };
    return service;
}]);