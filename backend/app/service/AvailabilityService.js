angular.module('app').factory('AvailabilityService', ['$http', 'EndpointService', function($http, EndpointService) {
    var service = {};
    service.getTeachers = function() {
        return $http({
            method: 'GET',
            url: EndpointService.getTeachers()
        });
    };
	service.toggleTeacher = function(teacher_id) {
        return $http({
            method: 'PATCH',
            url: EndpointService.toggleTeacher(teacher_id)
        });
    };
    return service;
}]);