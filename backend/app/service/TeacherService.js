angular.module('app').factory('TeacherService', ['$http', 'EndpointService', function($http, EndpointService) {
    var service = {};
    service.getTeachers = function() {
        return $http({
            method: 'GET',
            url: EndpointService.getTeachers()
        });
    };
    service.getTeacher = function(id) {
        return $http({
            method: 'GET',
            url: EndpointService.getTeacher(id)
        });
    };
    service.deleteTeacher = function(id) {
        return $http({
            method: 'DELETE',
            url: EndpointService.deleteTeacher(id)
        });
    };
    service.createTeacher = function(teacher) {
        return $http({
            method: 'PUT',
            url: EndpointService.createTeacher(),
            data: teacher
        });
    };
    service.updateTeacher = function(teacher) {
        return $http({
            method: 'POST',
            url: EndpointService.updateTeacher(),
            data: teacher
        });
    };
    return service;
}]);