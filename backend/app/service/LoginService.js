angular.module('app').factory('LoginService', ['$http', '$q', 'EndpointService', 'TokenService', function($http, $q, EndpointService, TokenService) {
    var service = {};

    var clientId = "1";
    var clientSecret = "abcd";
    var grantType = "password";

    service.login = function(username, password) {
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url : EndpointService.login(),
            data : {
                username : username,
                password : password,
                grantType : grantType,
                clientId: clientId,
                clientSecret : clientSecret
            }
        }).then(function success(response) {
            var token = response.data;
            TokenService.setToken(token.accessToken);
            TokenService.setRefresh(token.refreshToken);
            deferred.resolve();
        }, function error(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    };

    service.refreshToken = function(refreshToken) {
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: EndpointService.login(),
            data : {
                grantType : 'refreshToken',
                clientId : clientId,
                clientSecret : clientSecret,
                refreshToken : refreshToken
            }
        }).then(function success(response) {
            var token = response.data;
            TokenService.setToken(token.accessToken);
            TokenService.setRefresh(token.refreshToken);
            deferred.resolve();
        }, function error(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    };

    return service;
}]);