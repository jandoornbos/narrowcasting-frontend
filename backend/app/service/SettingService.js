angular.module('app').factory('SettingService', ['$http', 'EndpointService', function($http, EndpointService) {
    var service = {};
    service.getSettings = function() {
        return $http({
            method: 'GET',
            url: EndpointService.getSettings()
        });
    };
    service.getSetting = function(id) {
        return $http({
            method: 'GET',
            url: EndpointService.getSetting(id)
        });
    };
    service.updateSetting = function(id, setting) {
        return $http({
            method: 'PUT',
            url: EndpointService.updateSetting(id),
            data: setting
        });
    };
    return service;
}]);