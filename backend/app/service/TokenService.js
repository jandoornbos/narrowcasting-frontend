angular.module('app').factory('TokenService', ['$localStorage', function ($localStorage) {
    var service = {};

    service.getToken = function () {
        return $localStorage.accessToken;
    };
    service.setToken = function (token) {
        $localStorage.accessToken = token;
    };
    service.getRefresh = function() {
        return $localStorage.refreshToken;
    };
    service.setRefresh = function(token) {
        $localStorage.refreshToken = token;
    };
    service.removeToken = function() {
        $localStorage.$reset();
    };

    return service;
}]);