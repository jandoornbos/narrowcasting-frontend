'use strict';
var app = angular.module('app', ['ngRoute', 'ngStorage', 'ngSanitize', 'ui.router', 'textAngular', 'ui-notification']);
app.config(['$routeProvider', '$stateProvider', '$localStorageProvider', function($routeProvider, $stateProvider, $localStorageProvider) {
    $routeProvider.otherwise('/');
    $stateProvider
        .state('login', {
            url: '',
            views: {
                'body' : {
                    templateUrl: 'partials/login.html',
                    controller: 'LoginCtrl as vm'
                },
                'background' : {
                    templateUrl: 'partials/background.html',
                    controller: 'BackgroundCtrl as vm'
                }
            }
        })
		.state('portal', {	// first home to catch empty last url
            views: {
                'header' : {
                    templateUrl : 'partials/header.html',
                    controller : 'HeaderCtrl as vm'
                },
                'body' : {
                    templateUrl : 'partials/home.html',
                    controller : 'AvailabilityCtrl as ct'
                },
                'background' : {
                    templateUrl: 'partials/background.html',
                    controller: 'BackgroundCtrl as vm'
                }
            }
		})
		.state('portal.home', {	//second home to catch #/ at the end of the urls
			url: '/',
            views: {
                'body@' : {
                    templateUrl : 'partials/home.html',
                    controller : 'AvailabilityCtrl as ct'
                }
            }
		})
		.state('portal.message', {
            url : '/message',
            views: {
                'body@' : {
                    templateUrl : 'partials/messages.html',
                    controller : 'MessageCtrl as vm'
                }
            }
        })
        .state('portal.message.create', {
            url : '/create',
            views: {
                'body@' : {
                    templateUrl : 'partials/message.edit.html',
                    controller: 'MessageEditCtrl as vm'
                }
            }
        })
        .state('portal.message.edit', {
            url : '/:id',
            views: {
                'body@' : {
                    templateUrl : 'partials/message.edit.html',
                    controller: 'MessageEditCtrl as vm'
                }
            }
        })
		.state('portal.event', {
            url : '/event',
            views: {
                'body@' : {
                    templateUrl : 'partials/events.html',
                    controller : 'EventCtrl as vm'
                }
            }
        })
        .state('portal.event.create', {
            url : '/create',
            views: {
                'body@' : {
                    templateUrl : 'partials/event.edit.html',
                    controller: 'EventEditCtrl as vm'
                }
            }
        })
        .state('portal.event.edit', {
            url : '/{id}',
            views: {
                'body@' : {
                    templateUrl : 'partials/event.edit.html',
                    controller: 'EventEditCtrl as vm'
                }
            }
        })
		.state('portal.teacher', {
            url : '/teacher',
            views: {
                'body@' : {
                    templateUrl : 'partials/teachers.html',
                    controller : 'TeacherCtrl as vm'
                }
            }
        })
        .state('portal.teacher.create', {
            url : '/create',
            views: {
                'body@' : {
                    templateUrl : 'partials/teacher.edit.html',
                    controller: 'TeacherEditCtrl as vm'
                }
            }
        })
        .state('portal.teacher.edit', {
            url : '/{id}',
            views: {
                'body@' : {
                    templateUrl : 'partials/teacher.edit.html',
                    controller: 'TeacherEditCtrl as vm'
                }
            }
        })
		.state('portal.setting', {
            url : '/setting',
            views: {
                'body@' : {
                    templateUrl : 'partials/settings.html',
                    controller : 'SettingCtrl as vm'
                }
            }
        })
        .state('portal.setting.edit', {
            url : '/{id}',
            views: {
                'body@' : {
                    templateUrl : 'partials/setting.edit.html',
                    controller : 'SettingEditCtrl as vm'
                }
            }
        })
        .state('portal.user', {
            url: '/user',
            views : {
                'body@' : {
                    templateUrl : 'partials/user.html',
                    controller : 'UserCtrl as vm'
                }
            }
        })
        .state('portal.user.create', {
            url : '/create',
            views : {
                'body@' : {
                    templateUrl : 'partials/user.edit.html',
                    controller : 'UserEditCtrl as vm'
                }
            }
        })
        .state('portal.user.edit', {
            url : '/{id}',
            views: {
                'body@' : {
                    templateUrl : 'partials/user.edit.html',
                    controller : 'UserEditCtrl as vm'
                }
            }
        });
    $localStorageProvider.setKeyPrefix('narrowcasting');
}]);
app.run(function($rootScope, TokenService, $state) {
    $rootScope.$on('$stateChangeStart', function (event, toState) {
        if (toState.name.indexOf("login") === -1) {
            if (TokenService.getToken() == undefined) {
                event.preventDefault();
                $state.go('login');
            }
        }
    });
});