angular.module('app').directive('datePicker', ['$parse', '$timeout', function($parse, $timeout) {
    var directive = {};
    directive.link = function(scope, element, attrs) {
        var ngModel = $parse(attrs.ngModel);
        element.datetimepicker({
            format : 'DD-MM-YYYY HH:mm'
        }).on('dp.change', function(date, oldDate) {
            scope.$apply(function(scope) {
                ngModel.assign(scope, date.date);
            });
        });

        scope.$watch(attrs.ngModel, function () {
            $timeout(function() {
                element.data("DateTimePicker").date(new Date(ngModel(scope)));
            }, 0, false);
        });
    };
    return directive;
}]);