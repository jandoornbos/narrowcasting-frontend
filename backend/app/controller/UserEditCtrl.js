angular.module('app').controller('UserEditCtrl', ['$state', 'UserService', 'Notification', function($state, UserService, Notification) {
    var vm = this;
    vm.user = {};
    vm.addOrEdit = "toevoegen";
    vm.userId = $state.params.id;

    if (vm.userId == undefined) {
        // New user
    } else {
        vm.addOrEdit = "aanpassen";
        UserService.getUser(vm.userId).then(function success(response) {
            vm.user = response.data;
        }, function failure(response) {
            Notification.error('Kon gebruiker niet ophalen.');
        });
    }

    vm.saveUser = function() {
        vm.submitted = true;
        if (vm.form.$valid) {
            if (vm.userId == undefined) {
                createUser();
            } else {
                updateUser();
            }
        } else {
            Notification.error('Er zijn velden verplicht.');
        }
    };

    function createUser() {
        UserService.createUser(vm.user).then(function success(response) {
            Notification.success('Gebruiker is aangemaakt.');
            $state.go('portal.user');
        }, function failure(response) {
            Notification.error('Kon gebruiker niet aanmaken.');
        });
    }

    function updateUser() {
        UserService.updateUser(vm.userId, vm.user).then(function success(response) {
            Notification.success('Gebruiker is aangepast');
            $state.go('portal.user');
        }, function failure(response) {
            Notification.error('Gebruiker kon niet worden aangepast.');
        });
    }
}]);