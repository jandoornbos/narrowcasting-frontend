angular.module('app').controller('TeacherCtrl', ['TeacherService', 'Notification', function(TeacherService, Notification) {
    var vm = this;

    TeacherService.getTeachers().then(function success(response) {
        vm.teachers = response.data;
    }, function failure(response) {
        Notification.error('Kon docenten niet ophalen.');
    });
}]);