angular.module('app').controller('EventCtrl', ['EventService', function(EventService) {
    var vm = this;
	
    EventService.getEvents(1).then(function success(response) {
        vm.activeEvents = response.data;
    });
	EventService.getEvents(0).then(function success(response) {
        vm.inactiveEvents = response.data;
    });
}]);