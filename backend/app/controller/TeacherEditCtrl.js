angular.module('app').controller('TeacherEditCtrl', ['$state', 'TeacherService', 'Notification', function($state, TeacherService, Notification) {
    var vm = this;
	vm.addOrEdit = "toevoegen";
    vm.teacher = {};

	if ($state.params.id === undefined) {
        // Create new teacher
		
    } 
	else {
		vm.addOrEdit = "aanpassen";
        vm.teacherId = $state.params.id;
        TeacherService.getTeacher(vm.teacherId).then(function success(response) {
            vm.teacher = response.data;
        }, function failure(response) {
            Notification.error('Kon docent niet ophalen.');
        });
    }

	
    vm.saveTeacher = function() {
        vm.submitted = true;
        if (vm.form.$valid) {
            TeacherService.updateTeacher(vm.teacher).then(function success(response) {
                Notification.success('Docent is aangepast.');
				$state.go('portal.teacher');
			}, 
			function failure(response) {
				Notification.error('Kon docent niet updaten.');
			});
        }
    };

    function createTeacher() {
        TeacherService.createTeacher(vm.teacher).then(function success(response) {
            Notification.success('Docent is aangemaakt.');
            $state.go('portal.teacher');
        }, function failure(response) {
            Notification.error('Kon docent niet aanmaken.');
        });
    }

    vm.deleteTeacher = function() {
        TeacherService.deleteTeacher($state.params.id).then(function success(response) {
            Notification.success('Docent is verwijdert.');
            $state.go('portal.teacher');
        }, function failure(response) {
            Notification.error('Kon docent niet verwijderen.');
        });
    }
}]);