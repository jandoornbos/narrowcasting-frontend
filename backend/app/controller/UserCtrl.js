angular.module('app').controller('UserCtrl', ['UserService', 'Notification', function(UserService, Notification) {
    var vm = this;

    UserService.getUsers().then(function success(response) {
        vm.users = response.data;
    }, function failure(response) {
        Notification.error('Kon gebruikers niet ophalen.');
    });
}]);