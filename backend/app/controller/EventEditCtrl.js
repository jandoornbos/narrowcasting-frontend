angular.module('app').controller('EventEditCtrl', ['$state', 'EventService', 'Notification', function($state, EventService, Notification) {
    var vm = this;
    vm.event = {};
    vm.eventId = $state.params.id;

    if (vm.eventId == undefined) {
        // This is a new event
    } else {
        EventService.getEvent(vm.eventId).then(function success(response) {
            vm.event = response.data;
        }, function failure(response) {
            Notification.error('Kon evenement niet ophalen.');
            $state.go('portal.event');
        });
    }

    vm.saveEvent = function() {
        vm.submitted = true;
        if (vm.form.$valid) {
            if (vm.eventId == undefined) {
                createEvent();
            } else {
                updateEvent();
            }
        } else {
            Notification.error('Alle velden zijn verplicht');
        }
    };

    vm.deleteEvent = function() {
        EventService.deleteEvent(vm.eventId).then(function success(response) {
            $state.go('portal.event');
        }, function failure(response) {
            Notification.error('Kon evenement niet verwijderen.');
        });
    };

    function createEvent() {
        EventService.createEvent(vm.event).then(function success(response) {
            Notification.success('Het evenement is aangemaakt');
            $state.go('portal.event');
        }, function failure(response) {
            Notification.error('Kon evenement niet aanmaken.');
        });
    }

    function updateEvent() {
        EventService.updateEvent(vm.eventId, vm.event).then(function success(response) {
            Notification.success('Het evenement is aangepast');
            $state.go('portal.event');
        }, function failure(response) {
            Notification.error('Evenement kon niet worden aangepast.');
        });
    }
}]);