angular.module('app').controller('SettingCtrl', ['SettingService', 'Notification', function(SettingService, Notification) {
    var vm = this;

    SettingService.getSettings().then(function success(response) {
        vm.settings = response.data;
    }, function failure(response) {
        Notification.error('Kon instellingen niet ophalen.');
    });
}]);