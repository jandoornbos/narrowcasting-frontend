angular.module('app').controller('MessageCtrl', ['MessageService', 'Notification', function(MessageService, Notification) {
    var vm = this;
	
    MessageService.getMessages(1).then(function success(response) {
        vm.activeMessages = response.data;
    }, function failure(response) {
        Notification.error('Kon berichten niet ophalen.');
    });
	MessageService.getMessages(0).then(function success(response) {
        vm.inactiveMessages = response.data;
    }, function failure(response) {
        Notification.error('Kon verlopen berichten niet ophalen.');
    });
}]);