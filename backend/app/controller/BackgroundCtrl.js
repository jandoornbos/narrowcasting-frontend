angular.module('app').controller('BackgroundCtrl', ['$state', function($state) {
	var vm = this;
	vm.backgroundClass = "portalBackground";
	if($state.current.name == "login") {
		vm.backgroundClass = "loginBackground";
	}
}]);