angular.module('app').controller('SettingEditCtrl', ['$state', 'SettingService', 'Notification', function($state, SettingService, Notification) {
    var vm = this;
    vm.setting = {};
	vm.settingId = $state.params.id;
	SettingService.getSetting(vm.settingId).then(function success(response) {
		vm.setting = response.data;
	}, function failure(response) {
		Notification.error('Kon instelling niet ophalen.');
	});
	
    vm.saveSetting = function() {
        vm.submitted = true;
        if (vm.form.$valid) {
            SettingService.updateSetting(vm.settingId, vm.setting).then(function success(response) {
				Notification.success('Instelling opgeslagen.');
            	$state.go('portal.setting');
			}, function failure(response) {
				Notification.error('Kon instelling niet opslaan.');
			});
        }
    };
}]);