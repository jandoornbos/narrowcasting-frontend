angular.module('app').controller('AvailabilityCtrl',['AvailabilityService', '$element', function(AvailabilityService, $element) {
    var vm = this;

	// Revert the loginscreen id change
	$element[0].parentNode.id = "screen";
	
	AvailabilityService.getTeachers().then(function success(response) {
        vm.teachers = response.data;
    });
	vm.toggleTeacher = function($event, teacher){
		// Toggle the availability in the DB
		AvailabilityService.toggleTeacher(teacher.id);
		
		// Do some visual stuff to the Availablity list
		var t_element = angular.element($event.target);
		if (t_element.hasClass("list-group-item-danger")) {
			t_element.removeClass("list-group-item-danger");
			t_element.addClass("list-group-item-success");
			t_element.children().first().removeClass('fa-remove');
			t_element.children().first().addClass('fa-check');
		}
		else {
			t_element.removeClass("list-group-item-success");
			t_element.addClass("list-group-item-danger");
			t_element.children().first().removeClass('fa-check');
			t_element.children().first().addClass('fa-remove');
		}
	};
}]);