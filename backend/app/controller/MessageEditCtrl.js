angular.module('app').controller('MessageEditCtrl', ['$state', 'MessageService', 'Notification', function($state, MessageService, Notification) {
    var vm = this;
    vm.message = {};
	vm.addOrEdit = "toevoegen";
    vm.messageId = $state.params.id;

    if ($state.params.id === undefined) {
        // Create new message
    } else {
		vm.addOrEdit = "aanpassen";
        vm.messageId = $state.params.id;
        MessageService.getMessage(vm.messageId).then(function success(response) {
            vm.message = response.data;
        }, function failure(response) {
            Notification.error('Kon bericht niet ophalen');
            $state.go('portal.message');
        });
    }

    vm.saveMessage = function() {
        vm.submitted = true;
        if (vm.form.$valid) {
            // This is a new message
            if (vm.messageId == undefined) {
                createMessage();
            } else {
                // Update an message
                updateMessage();
            }
        } else {
            Notification.error('Alle velden zijn verplicht.');
        }
    };

    function createMessage() {
        MessageService.createMessage(vm.message).then(function success(response) {
            Notification.success('Nieuw bericht is gemaakt.');
            $state.go('portal.message');
        }, function failure(response) {
            Notification.error('Kon bericht niet aanmaken.');
        });
    }

    function updateMessage() {
        MessageService.updateMessage(vm.messageId, vm.message).then(function success(response) {
            Notification.success('Bericht is aangepast.');
            $state.go('portal.message');
        }, function failure(response) {
            Notification.error('Bericht kon niet worden aangepast.');
        });
    }

    vm.deleteMessage = function() {
        MessageService.deleteMessage($state.params.id).then(function success(response) {
            Notification.success('Bericht is verwijdert.');
            $state.go('portal.message');
        }, function failure(response) {
            Notification.error('Kon bericht niet verwijderen.');
        });
    }
}]);