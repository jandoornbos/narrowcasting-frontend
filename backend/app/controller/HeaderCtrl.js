angular.module('app').controller('HeaderCtrl', ['$state', 'TokenService', function($state, TokenService) {
    var vm = this;

    vm.logout = function() {
        TokenService.removeToken();
        $state.go('login');
    }
}]);