angular.module('app').controller('LoginCtrl', ['LoginService', 'TokenService', '$state', '$element', 'Notification', function(LoginService, TokenService, $state, $element, Notification) {
    var vm = this;
    vm.username = "";
    vm.password = "";
	
	//Change the #screen container to #loginScreen for css reasons
	$element[0].parentNode.id = "loginScreen";

    if (TokenService.getToken != undefined) {
        // Token found, continue
        $state.go('portal.home');
    }

    vm.login = function() {
        LoginService.login(vm.username, vm.password).then(function success() {
            $state.go('portal.home');
        }, function failure(response) {
            Notification.error(response.data.errorMessage);
        });
    };
}]);